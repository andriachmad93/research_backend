package main

import "fmt"

func main() {
	var fruits = []string{"apple", "grape", "banana", "melon"}
	var newFruits = fruits[0:3] // tiga elemen kalo [0:0] kosong, [] error

	fmt.Println(newFruits)

	var newFruit = fruits[:] //muncul semua

	fmt.Println(newFruit)

	var newFruita = fruits[2:] //muncul dr elemen 2

	fmt.Println(newFruita)

	var newFruitq = fruits[:2] //muncul dr awal sampai 2

	fmt.Println(newFruitq)
}
