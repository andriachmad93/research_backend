package main

import "fmt"
import "regexp"

func main() {
	var text = "banana burger soup"
	var regex, err = regexp.Compile(`[a-z]+`)

	if err != nil {
		fmt.Println(err.Error())
	}

	var res1 = regex.FindAllString(text, 2)
	fmt.Printf("%#v \n", res1)
	// ["banana", "burger"]

	var res2 = regex.FindAllString(text, -1)
	fmt.Printf("%#v \n", res2)
	// ["banana", "burger", "soup"]

	var regex1, _ = regexp.Compile(`[a-z]+`)

	var isMatch = regex1.MatchString(text)
	fmt.Println(isMatch)

	var str = regex1.FindString(text)
	fmt.Println(str)

	var idx = regex1.FindStringIndex(text)
	fmt.Println(idx)
	// [0, 6]

	var str1 = text[0:6]
	fmt.Println(str1)

	var str3 = regex.FindAllString(text, -1)
	fmt.Println(str3)
	// ["banana", "burger", "soup"]

	var str2 = regex.FindAllString(text, 1)
	fmt.Println(str2)

	var str4 = regex1.ReplaceAllString(text, "potato")
	fmt.Println(str4)

	var str5 = regex1.ReplaceAllStringFunc(text, func(each string) string {
		if each == "burger" {
			return "potato"
		}
		return each
	})
	fmt.Println(str5)

	var str6 = regex1.Split(text, -1)
	fmt.Printf("%#v \n", str6)
}
