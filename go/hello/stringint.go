package main

import "fmt"
import "strconv"

func main() {
	var str = "124"
	var num, err = strconv.Atoi(str)
	var num1, err1 = strconv.ParseFloat(str, 32)

	if err == nil {
		fmt.Println(num) // 124
	}

	if err1 == nil {
		fmt.Println(num1) // 124
	}

	var str1 = strconv.Itoa(num)

	fmt.Println(str1)
}
