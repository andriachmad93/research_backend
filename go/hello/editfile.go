package main

import "os"
import "io"
import "fmt"

func readFile() {
	// buka file
	var path = "/Users/achmadandriansyah/Documents/test.txt"
	var file, err = os.OpenFile(path, os.O_RDONLY, 0644)
	checkError(err)
	defer file.Close()

	// baca file
	var text = make([]byte, 1024)
	for {
		n, err := file.Read(text)
		if err != io.EOF {
			checkError(err)
		}
		if n == 0 {
			break
		}
	}
	fmt.Println(string(text))
	checkError(err)
}

func main() {
	readFile()
}
