package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

type Person struct{
	Name string
	Phone string
}

func main()  {
	session, err := mgo.Dial("mongodb://admin:admin@ds123796.mlab.com:23796/hacktivcash")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("hacktivcash").C("people")
	err = c.Insert(&Person{"Ale", "021 012345"}, &Person{"Cla", "022 012345"})
	if err != nil {
		log.Fatal(err)
	}

	result := Person{}
	err = c.Find(bson.M{"name": "Ale"}).One(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Phone:", result.Phone)
}